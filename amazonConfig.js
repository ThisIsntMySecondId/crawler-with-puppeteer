config = {
    url: 'https://www.amazon.in/s?k=nokia+6.1%2B&ref=nb_sb_noss_2',
    productLinks: '.a-link-normal.a-text-normal',
    requiredInfo: {
        title: {
            type: 'text',
            location: '#productTitle',
            postprocessing: null,
        },
        reviews: {
            type: 'text',
            location: '#acrCustomerReviewText',
            postprocessing: str => str.split(' ')[0],
        },
        questionsAnswered: {
            type: 'text',
            location: 'a#askATFLink .a-size-base',
            postprocessing: str => str.split(' ')[0],
        },
        originalPrice: {
            type: 'text',
            location: '.priceBlockStrikePriceString.a-text-strike',
            postprocessing: null,
        },
        discountedPrice: {
            type: 'text',
            location: '#priceblock_ourprice',
            postprocessing: null,
        },
        discount: {
            type: 'text',
            location: '.priceBlockSavingsString',
            postprocessing: null,
        },
        productImage: {
            type: 'image',
            location: '#imgTagWrapperId img',
            postprocessing: null,
            save: true,
            saveLocation: './amazonProductImages',
        },
    },
    dataSaveLocation: './amazon.json',
}

module.exports = config;