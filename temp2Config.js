const config = {
    url : 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&is_v=1',
    scope : '.dealblock_thump',
    mainData : {
        title: 'h1',
        price: 'h2 | trim',
        link: '.thump_img a@href',
        image: '.thump_img a img@src',
     },
}
module.exports = config;


// todo:  make it available for this config
// todo: make it available for single page as well as multipage (i.e. two pages) both