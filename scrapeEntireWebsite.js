const puppeteer = require('puppeteer');

let scrape = async (reqProducts) => {
    const browser = await puppeteer.launch({ headless: false });
    const result = [];
    startingUrl = 'http://books.toscrape.com/index.html';
    nextUrl = '';
    totproducts = 0;

    do {
        //create root page
        //set root page url to next link
        const root = await browser.newPage();
        await root.goto(nextUrl || startingUrl);
        startingUrl = '';

        // scrape entire content
        const allUrls = await root.evaluate(() => {
            let urls = Array.from(document.querySelectorAll('article div.image_container a'));
            return urls.map(a => a.href);
        });
        
        await root.waitFor(1000);
        for (url of allUrls) {
            const page2 = await browser.newPage();
            await page2.goto(url);
            await page2.waitFor(1000);
            const data = await page2.evaluate(() => {
                let title = document.querySelector('h1').innerText;
                let price = document.querySelector('.price_color').innerText;
                let stock = document.querySelector('p.instock.availability').innerText;
                stock = stock.replace(' In stock ', '').replace(' available', '').replace(/[()]/g, '');
                return { title, price, stock };
            });
            // close individual product(book) page
            page2.close();
            console.log(data, ' => ', totproducts++);

            if (totproducts > reqProducts) {
                return result;
            }
            // push to result
            result.push(data);
        }
        
        //locate next url
        //set next url to next link
        nextUrl = await root.evaluate(() => {
            let nextBtn = document.querySelector('ul.pager li.next a');
            if (nextBtn) return nextBtn.href;
            else return '';
        });
        
        //close the current root page
        //goto next page : done at starting of the loop
        root.close();

    }
    while (nextUrl);
    // rinse n repeat

    return result


};

scrape(30).then((value) => {
    console.log(value); // Success!
});

//lementHandle.click
//article div.image_container a