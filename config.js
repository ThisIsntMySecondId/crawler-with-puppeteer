config = {
    url: 'http://books.toscrape.com/',
    productLinks: 'article div.image_container a',
    requiredInfo: {
        title: {
            type: 'text',
            location: 'h1',
            postprocessing: null,
        },
        price: {
            type: 'text',
            location: '.price_color',
            postprocessing: null,
        },
        stock: {
            type: 'text',
            location: 'p.instock.availability',
            postprocessing: str => str.replace(' In stock ', '').replace(' available', '').replace(/[()]/g, ''),
        },
        bookImage: {
            type: 'image',
            location: '#product_gallery img',
            postprocessing: null,
            save: 'false',
            saveLocation: './images/',
        },
        bookImage2: {
            type: 'image',
            location: '#product_gallery img',
            postprocessing: null,
            save: 'false',
            saveLocation: './images2/',
        },
    },
    dataSaveLocation: './data.json',
}

// console.log(Object.entries(config.requiredInfo)[2][1]);
// console.log('\x1b[44m\x1b[37m %s \x1b[0m', 'image to be saved');

// console.log(path.join( config.requiredInfo.bookImage.saveLocation + 'ssss' + '.png'))

module.exports = config;