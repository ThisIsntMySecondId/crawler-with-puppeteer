config = {
    url: 'https://www.flipkart.com/search?q=redmi%20a2&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off',
    productLinks: 'a._31qSD5',
    requiredInfo: {
        title: {
            type: 'text',
            location: '._35KyD6',
            postprocessing: null,
        },
        ratings: {
            type: 'text',
            location: '._38sUEc',
            postprocessing: str => str.split(' ')[0],
        },
        Reviews: {
            type: 'text',
            location: '._38sUEc',
            postprocessing: str => str.split('&')[1].split(' ')[0].trim(),
        },
        originalPrice: {
            type: 'text',
            location: '._3auQ3N._1POkHg',
            postprocessing: null,
        },
        discountedPrice: {
            type: 'text',
            location: '._1vC4OE._3qQ9m1',
            postprocessing: null,
        },
        discount: {
            type: 'text',
            location: 'div.VGWI6T._1iCvwn',
            postprocessing: null,
        },
        productImage: {
            type: 'image',
            location: '._1Nyybr.Yun65Y.OGBF1g._30XEf0',
            postprocessing: null,
            save: true,
            saveLocation: './productImages',
        },
    },
    dataSaveLocation: './flipkart2.json',
}

module.exports = config;