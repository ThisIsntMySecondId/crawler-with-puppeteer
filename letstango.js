config = {
    url: 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&is_v=1',
    productLinks: '.dealblock_thump',
    requiredInfo: {
        title: {
            type: 'text',
            location: 'h1',
            postprocessing: null,
        },
        price: {
            type: 'text',
            location: 'h2 | trim',
            postprocessing: null,
        },
        link: {
            type: 'url',
            location: '.thump_img a',
            postprocessing: null,
        },
        discount: {
            type: 'text',
            location: 'div.VGWI6T._1iCvwn',
            postprocessing: null,
        },
        image: {
            type: 'image',
            location: '.thump_img a img',
            postprocessing: null,
        },
    },
    dataSaveLocation: './letstango.json',
}

module.exports = config;
