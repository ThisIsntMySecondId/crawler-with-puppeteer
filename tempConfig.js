// ? Flipkart Configuration

const config = {
    url : 'https://www.flipkart.com/search?q=shirts&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off',
    scope : '._3liAhj._1R0K0g',
    mainData : {
        title: 'a._2cLu-l',
        price: '._1vC4OE',
        link: 'a@href',
        image: 'a img@src',
     },
    innerDetails : {
        reviews: '._38sUEc',
        offers: '._3D89xM@html'
    },
    innerDetailsSource: "a@href",
    nextPage: '._1ypTlJ .fyt9Eu + a@href',
    // pageLimit: 2,
}
module.exports = config;