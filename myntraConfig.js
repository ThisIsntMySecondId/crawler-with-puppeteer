const puppeteer = require('puppeteer');

let scrape = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://www.myntra.com/shop/men');
    await page.waitFor(1000);

    //scrape
    const result = await page.evaluate(() => {
        let links = document.querySelectorAll('.navi-underline');

        links.map(link => link.innerText);

        return { link };
    });

    browser.close();
    return result;
};

scrape().then((value) => {
    console.log(value); // Success!
});

//lementHandle.click