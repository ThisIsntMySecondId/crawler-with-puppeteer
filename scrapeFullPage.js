const puppeteer = require('puppeteer');
const request = require('request');
const path = require('path');
const fs = require('fs');

//The configuration file
let config = null;
const requiredProducts = process.argv[3] || 5;
const configFile = process.argv[2];
if (!configFile) {
    console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'Config File Not Found Error: Please provide a configuration file');
    process.exit();
}
try {
    config = require(configFile);
} catch (err) {
    if (err.code == 'MODULE_NOT_FOUND') {
        console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'Config File Not Found Error: Please Make sure that you are entering correct confige file name');
        process.exit();
    }
}

for (info of Object.entries(config.requiredInfo)) {
    if (info[1].save) {
        fs.mkdir(info[1].saveLocation, (err) => {
            if (err.code == 'EEXIST') {
                console.log('\x1b[43m\x1b[30m %s \x1b[0m', 'Directory already exists: The program will keepon writing in the spicified directory');
            }
        });
    }
}

const saveImageToFile = (url, filename, callback) => {
    request.head(url, function (err, response, body) {
        request(url).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

let scrape = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    let totalProducts = 0;

    await page.goto(config.url);

    const allUrls = await page.evaluate((config) => {
        let urls = Array.from(document.querySelectorAll(config.productLinks));
        return urls.map(a => a.href);
    }, config).catch((err) => { console.log(err) });
    await page.waitFor(1000);
    // console.log(allUrls);

    result = [];

    for (url of allUrls) {
        const page2 = await browser.newPage();
        await page2.goto(url);
        await page2.waitFor(1000);
        const data = await page2.evaluate((config) => {
            let detail = {};
            for (info of Object.entries(config.requiredInfo)) {
                detail[info[0]] = document.querySelector(info[1].location);
                switch (info[1].type) {
                    case 'text': detail[info[0]] = document.querySelector(info[1].location).innerText; break;
                    case 'image': detail[info[0]] = document.querySelector(info[1].location).src; break;
                    case 'url': detail[info[0]] = document.querySelector(info[1].location).href; break;
                    default: detail[info[0]] = document.querySelector(info[1].location).innerText;
                }
            }
            return detail;
        }, config);
        page2.close();

        for (info of Object.entries(config.requiredInfo)) {
            if (info[1].postprocessing) {
                data[info[0]] = info[1].postprocessing(data[info[0]]);
            }

            if (info[1].type == 'image' && info[1].save) {
                // console.log('\x1b[40m\x1b[37m %s \x1b[0m', '---');
                // console.log('\x1b[44m\x1b[37m %s \x1b[0m', JSON.stringify(info[0]));
                await saveImageToFile(data[info[0]], path.join(info[1].saveLocation, data['title'].replace(/[/\:/"*?<>|]/g, '') + '.png'), (function (lll) {
                    return () => {
                        console.log('\x1b[44m\x1b[37m %s \x1b[0m', `${data['title'].replace(/[/\:/"*?<>|]/g, '')} image saved to ${lll}`);
                    }
                })(info[1].saveLocation));

            }
        }
        data['url'] = url;
        totalProducts = totalProducts + 1;
        console.log(data);
        console.log('\x1b[41m\x1b[37m %s \x1b[0m', `${totalProducts} => ${requiredProducts}`);
        result.push(data);
        if (totalProducts >= requiredProducts) {
            browser.close();
            return result;
        }
    }
    page.close();
    browser.close();
    return result;
};

scrape().then((value) => {
    console.log("***result***"); // Success!
    console.log(value); // Success!
    const writestream = fs.createWriteStream(config.dataSaveLocation);
    writestream.write(JSON.stringify(value));
    writestream.on('finish', () => {
        console.log('\x1b[44m\x1b[37m %s \x1b[0m', 'Successfully written to file');
    });

});